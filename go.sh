#!/bin/bash
drush make profiles/tng_kickstart/local.make -y

# Edit this to match your local
drush si tng --db-url='mysql://root:[edit]@localhost/d7' -y

# Give ourselves permission to edit the settings filey
chmod -R +w sites/default

# Edit this to match your local 
echo '$base_url = "http://local.d7.com";' >> sites/default/settings.php

# Not sure why this was created
rm -rf public\:

# Aurora Install
mkdir sites/all/themes/custom
mkdir sites/all/libraries
mkdir sites/all/modules/custom

# Generate a login
drush uli


